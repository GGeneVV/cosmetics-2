﻿using Cosmetics.Cart;
using Cosmetics.Common;
using Cosmetics.Contracts;
using Cosmetics.Products;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Cosmetics.Core.Engine
{
    public class CosmeticsFactory : ICosmeticsFactory
    {

        //Methods
        public ICategory CreateCategory(string name)
        {
            Category category = new Category(name);
            return category;
        }

        public Shampoo CreateShampoo(string name, string brand, decimal price, GenderType gender, uint milliliters, UsageType usage)
        {
            Shampoo shampoo = new Shampoo(name, brand, price, gender, milliliters, usage);
            return shampoo;
        }

        public Toothpaste CreateToothpaste(string name, string brand, decimal price, GenderType gender, IList<string> ingredients)
        {
            string ingredients1 = string.Join(", ", ingredients);
            Toothpaste toothpaste = new Toothpaste(name, brand, price, gender, ingredients1);
            return toothpaste;

        }
        public Cream CreateCream(string name, string brand, decimal price, GenderType gender, ScentType scent)
        {
            Cream cream = new Cream(name, brand, price, gender, scent);
            return cream;

        }
        public ShoppingCart CreateShoppingCart()
        {
            return new ShoppingCart();
        }
    }
}
