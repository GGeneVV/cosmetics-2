﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using System;

using System.Text;


namespace Cosmetics.Products
{
    public class Toothpaste : Product, IToothpaste
    {
        //FIELDS.....
        private string ingredients;

        //CONSTRUCTOR.....
        public Toothpaste(string name, string brand, decimal price, GenderType gender, string ingredients)
            : base(name, brand, price, gender)
        {
           
            this.Ingredients = ingredients;

        }
        //PROPERTIES
        public string Ingredients
        {
            get => this.ingredients;
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("Ingridients can not have null value");
                }
                this.ingredients = value;
                
            }
        }



        public override string Print()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"#{this.Name} {this.Brand}");
            sb.AppendLine($" #Price: ${this.Price}");
            sb.AppendLine($" #Gender: {this.Gender}");
            sb.AppendLine($" #Ingredients: {this.Ingredients}");
            sb.Append(" ===");
            return sb.ToString();
        }








    }
}