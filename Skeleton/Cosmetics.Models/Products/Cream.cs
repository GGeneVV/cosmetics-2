﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using System;
using System.Text;

namespace Cosmetics.Products
{
    public class Cream : Product,ICream
    {
        //Fields
        private ScentType scent;

        //Constructor
        public Cream(string name, string brand, decimal price, GenderType gender , ScentType scent)
            :base(name,brand,price,gender)
        {
           
            this.Scent = scent;
        }

        //Properties
        public ScentType Scent { get; set; }

        

        public override string Print()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine($"#{this.Name} {this.Brand}");
            stringBuilder.AppendLine($" #Price: ${this.Price}");
            stringBuilder.AppendLine($" #Gender: {this.Gender}");
            stringBuilder.AppendLine($" #Scent: {this.Scent}");
            stringBuilder.Append(" ===");
            return stringBuilder.ToString().TrimEnd();
        }
    }
}
