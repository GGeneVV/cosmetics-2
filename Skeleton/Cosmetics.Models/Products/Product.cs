﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Cosmetics.Products
{
    public abstract class Product : IProduct
    {
        //FIELDS...
        private string name;
        private string brand;
        private decimal price;
        private GenderType gender;

        //CONSTRUCTOR....
        public Product(string name, string brand, decimal price, GenderType gender)
        {
            this.Name = name;
            this.Brand = brand;
            this.Price = price;
            this.Gender = gender;
        }

        //PROPERTIES

        public  string Name
        {
            get => this.name;
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("Name can not be null");
                }
                if (value.Length<2 || value.Length>9)
                {
                    throw new ArgumentOutOfRangeException("Name lenght should be betwee 3 and 10 symbols");
                }

                this.name = value;
            }
        }
        public string Brand
        {
            get => this.brand;
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("Name can not be null");
                }
                if (value.Length < 3 || value.Length > 10)
                {
                    throw new ArgumentOutOfRangeException("Name lenght should be betwee 3 and 10 symbols");
                }
                this.brand = value;
            }
        }
        public decimal Price
        {
            get => this.price;
            set
            {
                if (value<0)
                {
                    throw new ArgumentOutOfRangeException("Price can not be negative");
                }
                this.price = value;
            }
        }




        public GenderType Gender { get; private set; }

        public abstract string Print();
    }
}
