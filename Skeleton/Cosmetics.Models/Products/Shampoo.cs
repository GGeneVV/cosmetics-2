﻿using Cosmetics.Common;
using Cosmetics.Contracts;
using System;
using System.Text;

namespace Cosmetics.Products
{
    public class Shampoo : Product,IShampoo
    {
        //FIELDS....
        private uint milliliters;
        private UsageType usage;

        //CONSTRUCTOR.....
        //name, brand, price, gender, milliliters, usage
        public Shampoo(string name, string brand, decimal price, GenderType gender
        , uint milliliters, UsageType usage) : base(name, brand, price, gender)
        {
            this.Milliliters = milliliters;
            this.Usage = usage;
        }

        public uint Milliliters
        {
            get => this.milliliters;
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Milliliters can not be negative");
                }
                this.milliliters = value;
            }
        }

        public UsageType Usage { get; set; }

        public override string Print()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"#{this.Name} {this.Brand}");
            sb.AppendLine($" #Price: ${this.Price}");
            sb.AppendLine($" #Gender: {this.Gender}");
            sb.AppendLine($" #Milliliters: {this.Milliliters}");
            sb.AppendLine($" #Usage: {this.Usage}");
            sb.Append(" ===");
            return sb.ToString();
        }

        
    }
}
