﻿using Cosmetics.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cosmetics.Contracts
{
    interface ICream : IProduct
    {
        ScentType Scent {get;set;}
    }
}
